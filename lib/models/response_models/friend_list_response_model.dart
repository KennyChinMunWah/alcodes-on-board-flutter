class FriendListResponseModel{
  int id;
  String first_name;
  String last_name;
  String avatar;
  String email;

  FriendListResponseModel({
      this.id,
      this.first_name,
      this.last_name,
      this.avatar,
      this.email
  });


}