import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermOfUseScreen extends StatefulWidget {
  @override
  _TermOfUseScreenState createState() => _TermOfUseScreenState();
}

class _TermOfUseScreenState extends State<TermOfUseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Terms Of Use')
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
        child: WebView(
          initialUrl:"https://generator.lorem-ipsum.info/terms-and-conditions",
          javascriptMode: JavascriptMode.unrestricted,
        ),
      )
    );
  }
}
