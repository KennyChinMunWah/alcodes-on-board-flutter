import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:flutter/material.dart';

class FriendDetailsScreen extends StatefulWidget {
  FriendListResponseModel arguments;
  FriendDetailsScreen({this.arguments});

  @override
  _FriendDetailsScreenState createState() => _FriendDetailsScreenState();
}

class _FriendDetailsScreenState extends State<FriendDetailsScreen> {
  FriendListResponseModel arguments;
  _FriendDetailsScreenState({this.arguments});

  @override
  Widget build(BuildContext context) {
    print(widget.arguments.id);
    return Scaffold(
        appBar: AppBar(
          title: Text("Friend Details"),
        ),
        body: Padding(
            padding: EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: CircleAvatar(
                      backgroundImage: NetworkImage(widget.arguments.avatar),
                      radius: 70.0),
                ),
                Divider(height: 90.0),
                Text('First Name : ',
                    style: TextStyle(
                        letterSpacing: 2.0,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold)),
                SizedBox(height: 10.0),
                Text(widget.arguments.first_name,
                    style: TextStyle(fontSize: 16.0)),
                SizedBox(height: 30.0),
                Text('Last Name : ',
                    style: TextStyle(
                        letterSpacing: 2.0,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold)),
                SizedBox(height: 10.0),
                Text(widget.arguments.last_name,
                    style: TextStyle(fontSize: 16.0)),
                SizedBox(height: 30.0),
                Text('Email : ',
                    style: TextStyle(
                        letterSpacing: 2.0,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold)),
                SizedBox(height: 10.0),
                Text(widget.arguments.email, style: TextStyle(fontSize: 16.0)),
              ],
            )));
  }
}
