import 'dart:async';
import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart' as appConst;
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:alcodes_on_board_flutter/repository/friend_list_repo.dart';
import 'package:alcodes_on_board_flutter/screens/friend_details_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

/// TODO Issue on this screen:
/// - Show list of my friends.
/// - Click list item go to my friend detail page.

class MyFriendList extends StatefulWidget {
  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {
  //defined a list to store return data
  String _loading = "loading";
  List<FriendListResponseModel> _userData = [];

  //calling api
  Future<void> getData() async {
    var alertDialogStatus = AppAlertDialogStatus.error;
    var alertDialogMessage = '';

    try {
      final repo = FriendListRepo();
      repo.friendListAsync().then((mData) {
        setState(() {
          _loading = "My Friend List";
          _userData = mData;
        });
      });
    } catch (ex) {
      Fimber.e('d;;Error request get list.', ex: ex);
      alertDialogMessage = '$ex';
    }

    if (alertDialogMessage.isNotEmpty) {
      final appAlertDialog = AppAlertDialog();
      appAlertDialog.showAsync(
          context: context,
          status: alertDialogStatus,
          message: alertDialogMessage);
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('$_loading'),
      ),
      body: ListView.builder(
        itemCount: _userData == null ? 0 : _userData.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: Card(
              child: ListTile(
                onTap: () {
                  Navigator.of(context).pushNamed(AppRouter.friendDetails,arguments: _userData[index]);
                },
                title: Padding(
                  padding: const EdgeInsets.all(appConst.kDefaultPadding),
                  child: Text(
                      '${_userData[index].first_name}           ${_userData[index].last_name}\n\n${_userData[index].email}',
                      style: TextStyle(
                        fontSize: 15.0,
                      )),
                ),
                leading: CircleAvatar(
                  radius: 40.0,
                  backgroundImage: NetworkImage(_userData[index].avatar),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
